Eluwka xD
Oto plik, który będzie z grubsza opisywał postępy pracy przy tworzeniu 
biblioteki. Będą tu umieszczane plany na dalszą pracę oraz co z tych 
planów wyszło. Gdy robota zostanie skończona, albo przynajmniej osiągnie 
się jakiś istotny punkt pracy, to dopisze się jeszcze instrukcję z 
cyklu: co jest czym, co za co odpowiada itp. Ale jak na razie jest to 
pieśń przyszłości. No więc do roboty XD

Jako pierwsze do zrobienia mamy:
-funkcja inicjalizująca rejestr MCR, czyli rejestr konfiguracyjny 
naszego kontrolera CAN
-funkcja inicjalizująca filtry, tutaj trzeba się zastanowić jeszcze w 
jakiej konkretnie formie ma to wyglądać, prawdopodobnie będzie to 
pojedyńcza funkcja, której będzie się używać każdorazowo, gdy będzie się 
chciało uruchomić jakiś nowy filtr
-funkcja inicjalizująca przerwania (nie wiem czy na pewno ma być)
-funkcja do wysyłania wiadomości - trzeba wziąć pod uwagę, że są trzy 
skrzynki nadawcze, a ponadto istnieje możliwość zaistnienia błędów, z 
którymi można walczyć np. przy pomocy wbudowanych w kontroler flag o 
błędach i skomunikowanych z nimi przerwań
-funkcja odbierająca wiadomości, tutaj możnaby zrobić dwie wersje, jedną 
opierającą się na przerwaniach, drugą zaś na zwykłym odpytywaniu

Nade wszystko, warto zobaczyć jak w LL są zrealizowane biblikoteki 
interfejsów komunikacyjnych i się na nich wzorować - tak chyba będzie 
czytelniej xD
