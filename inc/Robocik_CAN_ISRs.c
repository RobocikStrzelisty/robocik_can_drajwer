/*
 * Robocik_CAN_ISRs.c
 *
 *  Created on: 04.03.2020
 *      Author: Dell
 */


#include "Robocik_CAN_ISRs.h"

void Robocik_CAN_RX0_Handler(void)
{
	/*
	 * Tu pisz swoj� procedur� obs�ugi przerwania generowanego przez FIFO0, gdy odbierze ono
	 * wiadomo�c.
	 */
}

void Robocik_CAN_RX1_Handler(void)
{
	/*
	 * Tu pisz swoj� procedur� obs�ugi przerwania generowanego przez FIFO1, gdy odbierze ono
	 * wiadomo�c.
	 */
}
