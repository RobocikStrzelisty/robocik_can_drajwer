/*
 * Robocik_CAN_drajwer.c
 *
 *  Created on: 21.02.2020
 *      Author: Dell
 */

/*** Sekcja z inkludesami ***/

#include "Robocik_CAN_drajwer.h"

/*** Koniec sekcji z inkludesami ***/

/*** Sekcja z definicjami funkcji ***/


// Co robi: 	funkcja inicjalizacyjna kontrolera bxCAN w STM32F103, a dok�adniej
//				przechodzi w tryb inicjalizacji, ustawia rejestr kontrolny MCR
//				oraz rejestr bittajmingu BTR; dok�adnie dzia�anie b�dzie wyja�nione
//				na przyk�adzie i mo�e w jakim� kr�tkim tutorialu
// Co pobiera:	pobiera struktur� inicjalizacyjn� kontrolera CAN, kt�ra jest opisana
//				powy�ej
// Co zwraca:	je�li inicjalizacja przebieg�a pomy�lnie SUCCESS, je�li nie ERROR
ErrorStatus Robocik_CAN_Init(Robocik_CAN_InitTypeDef * initStruct)
{
	// podpinamy kontroler do zegara
	RCC->APB1ENR |= RCC_APB1ENR_CAN1EN;

	//przechodzimy do trybu inicjalizacji
	CAN1->MCR = 1;
	//oczekujemy na zatwierdzenie tego stanu przez kontroler
	// to mo�naby potem jako� poprawic, bo obecna forma wygl�da troch� gro�nie xD
	while(!Robocik_CAN_BB(CAN1->MSR, 0));

	// kto wie czy nie trzeba tu b�dzie u�yc wska�nika lub te� unii
	initStruct->niepisz1 = 0, initStruct->niepisz2 = 0, initStruct->niepisz3 = 0,
	initStruct->niepisz4 = 0, initStruct->niepisz5 = 0, initStruct->niepisz6 = 0;

	CAN1->MCR |= *((uint32_t *)initStruct);

	// tutaj ustawienia ze struktury inicjalizacyjnej s� wpisywane do rejestru BTR

	CAN1->BTR = *(((uint32_t *)initStruct)+1);
	// wychodzimy z trybu inicjalizacyjnego i czekamy na zatwierdzenie tego faktu w rejestrze MSR
	Robocik_CAN_BB(CAN1->MCR, 0) = 0;

	while(Robocik_CAN_BB(CAN1->MSR, 0));
	// poni�sze na ewentualno�c dodania obs�ugi b��d�w xD
	return SUCCESS;
}


// Co robi:		konfiguruje i w��cza okre�lony w strukturze inicjalizacyjnej filtr
// Co pobiera:	struktur� inicjalizacyjn� opisan� powy�ej
// Co zwraca:	status b��du
ErrorStatus Robocik_CAN_FilterInit(Robocik_CAN_FilterInitTypeDef *initStruct)
{
	// Najpierw nale�y wyzerowac odpowiedni bit FACTx w rejestrze FA1R, co sprawia,
	// �e zostaje zdezaktywowany i mo�liwa jest jego inicjalizacja
	CAN1->FA1R &= ~(1U << initStruct->filterNumber);
	// Wybieramy tryb filtru - maski lub podw�jnego identyfikatora
	if(initStruct->filterMode == Robocik_CAN_FilterMode_Identifier)
		CAN1->FM1R |= 1U << initStruct->filterNumber;
	else
		CAN1->FM1R &= ~(1U << initStruct->filterNumber);
	// Wybieramy skal� filtru - pojedy�czy 16bitowy lub podw�jny 32bitowy
	if(initStruct->filterScale == Robocik_CAN_FilterScale_Single)
		CAN1->FS1R |= 1U << initStruct->filterNumber;
	else
		CAN1->FS1R &= ~(1U << initStruct->filterNumber);
	// Wybieramy do kt�rego fifo przypiszemy nasz filtr
	if(initStruct->filterFIFOAssigment == Robocik_CAN_FilterFIFOAssigment_FIFO1)
		CAN1->FFA1R |= 1U << initStruct->filterNumber;
	else
		CAN1->FFA1R &= ~(1U << initStruct->filterNumber);
	// Zapisujemy warto�c identyfikatora i ew. maski do odpowiednich rejestr�w
	CAN1->sFilterRegister[initStruct->filterNumber].FR1 = ((uint32_t)initStruct->filterIdHigh << 16U
														| (uint32_t)initStruct->filterIdLow << 0U);
	CAN1->sFilterRegister[initStruct->filterNumber].FR2 = ((uint32_t)initStruct->filterMaskHigh << 16U
														| (uint32_t)initStruct->filterMaskLow << 0U);
	// No i teraz, w zale�no�ci od stanu pola aktywacji filtru w strukturze inicjalizacyjnej,
	// aktywujemy lub te� pozostawiamy nieaktywny
	CAN1->FA1R |= (initStruct->filterActivation ? (1U << initStruct->filterNumber) : 0U);
	// konfigurujemy filtry tak, by by�y przypisane do CAN1,
	// a nast�pnie zerujemy ostatni bit rejestru FMR, upewniaj�c si�, �e nie jeste�my w
	//trybie inicjalizacji
	CAN1->FMR = (28U << 8U) & 0xFFFFFFFEUL;

	return SUCCESS;
}

// Co robi:		wysy�a ramk� danych
// Co pobiera:	ramk� danych, kt�r� jest struktura opisana powy�ej
// Co zwraca:	je�li transmisja jest udana - SUCCESS, je�li przegrano arbitra� - arbitration
//				lost, je�li wyst�pi� b��d transmisji - transmision error detection
//				typ (enumeracyjny) stanu transmisji zosta�a opisana powy�ej, je�li wszystkie
//				skrzynki s� zaj�te - NoEmptyMailboxes
Robocik_CAN_TransmitState Robocik_CAN_Transmit(Robocik_CAN_DataFrame * dataFrame)
{

	// najsampierw szukamy pustej skrzynki nadawczej
	uint8_t whichMailbox = 0;
	while((!Robocik_CAN_BB(CAN1->TSR, CAN_TSR_TME0_Pos+whichMailbox)) && (whichMailbox++ < 3U));
	if(whichMailbox >= 3)
		return Robocik_CAN_TransmitState_NoEmptyMailboxes;
	// nast�pnie wype�niamy rejestr TDTR - kontroli d�ugo�ci wiadomo�ci i stempla czasowego
	CAN1->sTxMailBox[whichMailbox].TDTR = *(((uint32_t*)dataFrame)+1);
	// zapisujemy dane do wys�ania, pocz�wszy od rejestru dolnego, ko�cz�c na g�rnym
	CAN1->sTxMailBox[whichMailbox].TDLR = (*((uint32_t*)dataFrame->data));
	// tutaj ewentualnie mo�na dodac warunek wykonania zale�nie od tego, ile danych przesy�amy,
	// jednak, przynajmniej na razie wydaje mi si� on zb�dny
	CAN1->sTxMailBox[whichMailbox].TDHR = (*((uint32_t*)(&(dataFrame->data[4]))));
	// na koniec wpisujemy do rejestru TIR identyfikator wiadomo�ci oraz wysy�amy do
	// kontrolera ��danie wys�ania wiadomo�ci ustawiaj�c w nim bit TXRQ
	dataFrame->TransmitMailboxRequest = 1;

	CAN1->sTxMailBox[whichMailbox].TIR = *((uint32_t *)(dataFrame));

	/* No i teraz pytanie, czy chcemy czekac na wynik transmisji - alternatyw� jest
	 * zako�czenie dzia�ania funkcji bez wzgl�du na wynik i ew. dopisanie oddzielnych
	 * funkcji, kt�re zwraca�yby bity statusu. Na razie zrobi� to w formie oczekiwania.
	 */
	//czekamy a� kontroler poinformuje o ko�cu transmisji, tzn. na postawienie bitu RQCP
	// W gruncie rzeczy w dalszych implementacjach należy to wyjebać
	// bo na chuj mamy czekać aż skończy się transmisja, przeca nadawanie może trwać wieki XD
	/*while(!(CAN1->TSR & (1U << whichMailbox * 8U)));
	// a teraz sprawdzamy, kt�ry z bit�w stanu transmisji si� zapali� i zwracamy
	// odpowiedni� warto�c
	// najpierw TXOK
	if(CAN1->TSR & (1U << (whichMailbox * 8U + 1U)))
		return Robocik_CAN_TransmitState_Success;
	else if(CAN1->TSR & (1U << (whichMailbox * 8U + 2U)))
		return Robocik_CAN_TransmitState_ArbitrationLost;
	else if(CAN1->TSR & (1U << (whichMailbox * 8U + 3U)))
		return Robocik_CAN_TransmitState_TransmissionErrorDetection;

	return Robocik_CAN_TransmitState_StrangeErrorXD;*/

	// Zakomentowane, bo nie chcemy, by operacja wysylania blokowala bieg programu
	return Robocik_CAN_TransmitState_Success;
}


// Co robi:		odbiera ramk� danych z okre�lonego FIFO (mo�e byc 0 lub 1)
//				i zeruje flag� RFOMwyzwalaj�c miejsce w FIFO
// Co pobiera:	wska�nik na ramk� danych oraz numer fifo z kt�rego odbieramy dane
// Co zwraca:	SUCCESS je�li operacja si� powiedzie, ERROR je�li nie lub nie ma danych
//				do pobrania
Robocik_CAN_ReceiveState Robocik_CAN_Receive(Robocik_CAN_DataFrame * dataFrame, uint8_t whichFIFO)
{
	if(whichFIFO == 0)
	{
		if(!(CAN1->RF0R & 3U))
			return Robocik_CAN_ReceiveState_NoPendingMessages;
	}
	else
	{
		if(!(CAN1->RF1R & 3U))
			return Robocik_CAN_ReceiveState_NoPendingMessages;
	}
	if(!((CAN1->RF0R | CAN1->RF1R) & 3U))
	// odczytujemy identyfikator wiadomo�ci

	*((uint32_t *)(dataFrame)) = CAN1->sFIFOMailBox[whichFIFO].RIR;
	// tutaj zbieramy dane o ilo�ci bajt�w w wiadomo�ci, a tak�e o filtrze, kt�ry wiadomo�c
	// przesz�a
	*(((uint32_t *)(dataFrame))+1) = CAN1->sFIFOMailBox[whichFIFO].RDTR;

	(*((uint32_t*)dataFrame->data)) = CAN1->sFIFOMailBox[whichFIFO].RDLR;
	(*((uint32_t*)(&(dataFrame->data[4])))) = CAN1->sFIFOMailBox[whichFIFO].RDHR;

	// a tutaj dodatkowo zg�aszamy programowi, �e pierwsza wiadomo�c zosta�a ju� odczytana i mo�na j� wyrzucic
	// co pozwala nam wyj�c z przerwania
	if(whichFIFO == 1)
		CAN1->RF1R = CAN_RF1R_RFOM1;
	else
		CAN1->RF0R = CAN_RF0R_RFOM0;

	return Robocik_CAN_ReceiveState_Success;
}


// Co robi:		uruchamia kontroler
// Co pobiera:	nic
// Co zwraca:	nic
void Robocik_CAN_EnableNormalMode(void)
{
	CAN1->MCR &= 0xFFFFFFFCU;
}


// Co robi:		wchodzi w tryb u�pienia
// Co pobiera:	nic
// Co zwraca:	nic
void Robocik_CAN_EnableSleepMode(void)
{
	CAN1->MCR |= 1U << 1U;
}


// Co robi:		aktywuje odpowiedni filtr
// Co pobiera:	numer filtru (od zera oczywi�cie)
// Co zwraca:	nic
void Robocik_CAN_EnableFilter(uint8_t whichFilter)
{
	CAN1->FA1R |= 1U << whichFilter;
}


// Co robi:		w��cza przerwanie generowane przy otrzymaniu wiadomo�ci przez
//				okre�lone na wej�ciu FIFO, co wa�ne - obs�ug� przerwania trzeba jeszcze
//				potem w��czyc w NVICu - NVIC_EnableIRQ(CAN1_RX1_IRQn);
// Co pobiera:	pobiera pozycj� bitu, kt�ry odpowiada za dane przerwanie, bity te s�
//				opisane w CMSIS nast�puj�co: CAN_IER_FMPIEx, gdzie ko�cowa cyfra
//				mo�e wynosic 0 lub 1, zale�nie od tego, kt�re FIFO ma generowac przerwanie
// Co zwraca:	nic
void Robocik_CAN_EnableIT_FifoMessagePendingIT(uint8_t whichFIFO)
{
	CAN1->IER = (whichFIFO ? CAN_IER_FMPIE1 : CAN_IER_FMPIE0);
}

/*** Koniec sekcji z definicjami funkcji ***/

